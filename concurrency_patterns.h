#ifndef CONCURRENCY_PATTERNS_H
#define CONCURRENCY_PATTERNS_H

#include "abstract_pattern.h"

class ConcurrencyPatterns : public AbstractPattern
{
public:
    ConcurrencyPatterns() = default;
    ~ConcurrencyPatterns() = default;

    void showPatterns() const;
};

#endif // CONCURRENCY_PATTERNS_H
