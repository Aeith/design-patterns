#ifndef MULTITON_H
#define MULTITON_H

/**
 * Who needs a map of single instances of something ?
 * Stop using classes in that case...
 **/

#include <mutex>
#include <map>

class Multiton {

private:
    // Ctor Dtor
    Multiton() = default;
    ~Multiton() = default;

    /** Attributes **/
    static std::map<unsigned int, Multiton *> _instances;
    static std::mutex _mutex;

public:
    // Assign / Copy
    Multiton(Multiton & other) = delete;
    void operator=(const Multiton &) = delete;

    /** Methods **/
    static Multiton * Instance(unsigned int key);
};

#endif // MULTITON_H
