#ifndef BRIDGE_H
#define BRIDGE_H

#include <string>

class Implementation
{

public:
    Implementation() = default;
    virtual ~Implementation() = default;

    virtual std::string operationImplementation() const = 0;

};

class ConcreteImplementation1 : public Implementation
{

public:
    ConcreteImplementation1() = default;
    virtual ~ConcreteImplementation1() = default;

    virtual std::string operationImplementation() const override;

};

class ConcreteImplementation2 : public Implementation
{

public:
    ConcreteImplementation2() = default;
    virtual ~ConcreteImplementation2() = default;

    virtual std::string operationImplementation() const override;

};

class Abstraction
{

public:
    Abstraction(Implementation* impl) : _implementation(impl) {};
    virtual ~Abstraction() = default;

    virtual std::string operationAbstraction() const;

protected:
    Implementation* _implementation;

};

class ExtendedAbstraction : Abstraction
{

public:
    ExtendedAbstraction(Implementation* impl) : Abstraction(impl) {};
    virtual ~ExtendedAbstraction() = default;

    virtual std::string operationAbstraction() const;

};

#endif // BRIDGE_H
