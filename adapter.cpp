#include "adapter.h"

std::string Target::targetMethod(int param) const
{
    return std::to_string(param);
}

std::string Adaptee::adapteeMethod() const
{
    return "Target method";
}

std::string Adapter::targetMethod([[maybe_unused]] int param) const
{
    int intermediary = 0;
    for (size_t i = 0; i < _adaptee.adapteeMethod().size(); i++) {
        intermediary += static_cast<int>(_adaptee.adapteeMethod()[i]);
    }
    return std::to_string(intermediary);
}
