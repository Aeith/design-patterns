#ifndef ABSTRACTFACTORY_H
#define ABSTRACTFACTORY_H

#include <string>
class AbstractProductA {

public:
    explicit AbstractProductA() = default;
    virtual ~AbstractProductA() = default;

    virtual std::string productType() const = 0;
};

class AbstractProductB {

public:
    explicit AbstractProductB() = default;
    virtual ~AbstractProductB() = default;

    virtual std::string productType() const = 0;
};

class AbstractProductC {

public:
    explicit AbstractProductC() = default;
    virtual ~AbstractProductC() = default;

    virtual std::string productType() const = 0;
};

class AbstractFactory
{

public:
    explicit AbstractFactory() = default;
    virtual ~AbstractFactory() = default;

    virtual AbstractProductA* createProductA() const = 0;
    virtual AbstractProductB* createProductB() const = 0;
    virtual AbstractProductC* createProductC() const = 0;
};

class ConcreteProductAVariant1 : public AbstractProductA {

public:
    explicit ConcreteProductAVariant1() = default;
    virtual ~ConcreteProductAVariant1() = default;

    std::string productType() const override {
        return "Product A Variant 1";
    }
};

class ConcreteProductAVariant2 : public AbstractProductA {

public:
    explicit ConcreteProductAVariant2() = default;
    virtual ~ConcreteProductAVariant2() = default;

    std::string productType() const override {
        return "Product A Variant 2";
    }
};

class ConcreteProductBVariant1 : public AbstractProductB {

public:
    explicit ConcreteProductBVariant1() = default;
    virtual ~ConcreteProductBVariant1() = default;

    std::string productType() const override {
        return "Product B Variant 1";
    }
};

class ConcreteProductBVariant2 : public AbstractProductB {

public:
    explicit ConcreteProductBVariant2() = default;
    virtual ~ConcreteProductBVariant2() = default;

    std::string productType() const override {
        return "Product B Variant 2";
    }
};

class ConcreteProductCVariant1 : public AbstractProductC {

public:
    explicit ConcreteProductCVariant1() = default;
    virtual ~ConcreteProductCVariant1() = default;

    std::string productType() const override {
        return "Product C Variant 1";
    }
};

class ConcreteProductCVariant2 : public AbstractProductC {

public:
    explicit ConcreteProductCVariant2() = default;
    virtual ~ConcreteProductCVariant2() = default;

    std::string productType() const override {
        return "Product C Variant 2";
    }
};

class ConcreteFactory1 : public AbstractFactory {

public:
    explicit ConcreteFactory1() = default;
    virtual ~ConcreteFactory1() = default;

    virtual AbstractProductA* createProductA() const override { return new ConcreteProductAVariant1; }
    virtual AbstractProductB* createProductB() const override { return new ConcreteProductBVariant1; }
    virtual AbstractProductC* createProductC() const override { return new ConcreteProductCVariant1; }
};

class ConcreteFactory2 : public AbstractFactory {

public:
    explicit ConcreteFactory2() = default;
    virtual ~ConcreteFactory2() = default;

    virtual AbstractProductA* createProductA() const override { return new ConcreteProductAVariant2; }
    virtual AbstractProductB* createProductB() const override { return new ConcreteProductBVariant2; }
    virtual AbstractProductC* createProductC() const override { return new ConcreteProductCVariant2; }
};

#endif // ABSTRACTFACTORY_H
