#ifndef FLYWEIGHT_H
#define FLYWEIGHT_H

#include <iostream>
#include <map>
#include <string>

// Shared object to manage
class SharedObject
{

public:
    SharedObject(std::string name) : _name(name) {};
    ~SharedObject() = default;

    std::string toString() const {
        return "[" + _name + "]";
    }

    std::string getName() const { return _name; }

    long long toUUID() const;

private:
    std::string _name {""};

};

// Unique object
class UniqueObject
{

public:
    UniqueObject(int x, int y, int width, int height) : _x(x), _y(y), _width(width), _height(height) {};
    ~UniqueObject() = default;

    std::string toString() const {
        return "[(" + std::to_string(_x) + "," + std::to_string(_y) + "), " + std::to_string(_width) + "x" + std::to_string(_height) + "]";
    }

private:
    int _x {0};
    int _y {0};
    int _width {0};
    int _height {0};

};

// Manages HeavyObject
class Flyweight
{

public:
    Flyweight() = delete;
    Flyweight(const SharedObject* obj) : _sharedObject(new SharedObject(*obj)) {};
    Flyweight(const Flyweight& other) : _sharedObject(new SharedObject(*other._sharedObject)) {}
    ~Flyweight() { delete _sharedObject; }

    SharedObject* getObject() const { return _sharedObject; }
    // No setters/adders/removers as the flyweight is immutable once initialized

    std::string operation(const UniqueObject& uo) const
    {
        return "Flyweight; Displaying shared (" + _sharedObject->toString() + ") and unique (" + uo.toString() + ") states";
    }

private:
    SharedObject* _sharedObject {nullptr};

};

// Creates flyweights or return one
class FlyweightFactory
{

public:
    FlyweightFactory() = default;
    ~FlyweightFactory() {
        for (auto flyweight : _flyweights) {
            if (!flyweight.second) continue;
            delete flyweight.second;
        }
    };

    Flyweight* get(SharedObject* shObject);
    void list() const;

private:
    int getKey(SharedObject* shObject) const;

    std::unordered_map<long long, Flyweight*> _flyweights;

};

// Wrapper containing the flyweight > this is what is used by the client
class ComplexObject
{

public:
    ComplexObject(FlyweightFactory* factory, int x, int y, int width, int height, std::string name) :
        _uniqueObject(new UniqueObject(x, y, width, height)) {
        _flyweight = factory->get(new SharedObject(name));
        if (!_flyweight) {
            std::runtime_error("Could not generate a flyweight, stopping");
            exit(1);
        }
    }
    ~ComplexObject() {
        delete _uniqueObject;
    }

    std::string operation() const { return _flyweight->operation(*_uniqueObject); }

private:
    UniqueObject* _uniqueObject {nullptr};
    Flyweight* _flyweight {nullptr}; // Instead of SharedObject* that is shared accross multiple complex objects

};

#endif // FLYWEIGHT_H
