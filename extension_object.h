#ifndef EXTENSIONOBJECT_H
#define EXTENSIONOBJECT_H

#include <string>

class ExtendedObject
{

public:
    ExtendedObject() = default;
    virtual ~ExtendedObject() = default;

    std::string defaultMethod() const;

    int _value {13};
    bool _flag {false};

};

class Extension
{

public:
    Extension() = default;
    virtual ~Extension() = default;

    virtual std::string extendedMethod(ExtendedObject o) const = 0;

};

class ExtensionObject : public Extension
{

public:
    ExtensionObject() = default;
    virtual ~ExtensionObject() = default;

    std::string extendedMethod(ExtendedObject o) const;

};

#endif // EXTENSIONOBJECT_H
