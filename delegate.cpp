#include "delegate.h"

std::string Delegate::function() const
{
    return "Delegate function";
}

std::string Delegee::function() const
{
    return _delegate->function();
}
