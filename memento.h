#ifndef MEMENTO_H
#define MEMENTO_H

#include <iostream>
#include <vector>

struct State
{
    enum Rank { Apprentice, Commoner, Initiate, Expert, Master };

    Rank rank;
    int level;
    std::string name;
};

// Saving object
class Memento
{

public:
    Memento() = default;
    Memento(State state) : _state(state) {}
    virtual ~Memento() = default;

    virtual State getState() { return _state; }
    virtual void setState(State state) { _state = state; }

private:
    State _state;

};

// Saved object
class Originator
{

public:
    Originator(std::string name) { _state.name = name; };
    virtual ~Originator() = default;

    Memento* save() const { return new Memento(_state); }
    void restore(Memento* memento) { _state = memento->getState(); }

    void advance();
    void levelUp();

private:
    State _state;

};

// Controller
class Caretaker
{

public:
    Caretaker(Originator* originator) : _originator(originator) {};
    virtual ~Caretaker() {
        for (auto save : _saves) {
            delete save;
        }
    };

    void save();
    void undo();
    void list();

private:
    std::vector<Memento*> _saves = {};
    Originator* _originator {nullptr};

};

#endif // MEMENTO_H
