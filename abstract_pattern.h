#ifndef ABSTRACTPATTERN_H
#define ABSTRACTPATTERN_H

#include <iostream>
#include <vector>
#include <string>

class AbstractPattern
{

public:
    AbstractPattern() = default;
    virtual ~AbstractPattern() = default;

    /** Methods **/
    template <typename T>
    void showPattern(std::string pattern, std::vector<T> elements) const {
        for (size_t i = 0; i < elements.size(); i++) {
            if (i == 0) std::cout << "\n";
            std::cout << pattern << " : " << elements[i] << std::endl;
        }
    }

};

#endif // ABSTRACTPATTERN_H
