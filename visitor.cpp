#include "visitor.h"

std::string ConcreteComponentA::accept(Visitor* visitor) const
{
    return visitor->visitConcreteComponentA(this);
}

std::string ConcreteComponentA::functionA() const
{
    return "Component A member function";
}

std::string ConcreteComponentB::accept(Visitor* visitor) const
{
    return visitor->visitConcreteComponentB(this);
}

std::string ConcreteComponentB::functionB() const
{
    return "Component B member function";
}

std::string ConcreteVisitor1::visitConcreteComponentA(const ConcreteComponentA* component) const
{
    return "Visitor 1 - " + component->functionA();
}

std::string ConcreteVisitor1::visitConcreteComponentB(const ConcreteComponentB* component) const
{
    return "Visitor 1 - " + component->functionB();
}

std::string ConcreteVisitor2::visitConcreteComponentA(const ConcreteComponentA* component) const
{
    return "Visitor 2 - " + component->functionA();
}

std::string ConcreteVisitor2::visitConcreteComponentB(const ConcreteComponentB* component) const
{
    return "Visitor 2 - " + component->functionB();
}
