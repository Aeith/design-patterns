#include <iostream>

#include "factory.h"
#include "object_pool.h"

template <class T>
ObjectPool<T> * ObjectPool<T>::_instance{nullptr};

template <class T>
ObjectPool<T> * ObjectPool<T>::Instance() {
    if (ObjectPool::_instance == nullptr)
        _instance = new ObjectPool();

    return _instance;
}

template <class T>
T* ObjectPool<T>::getResource()
{
    if (this->_resources.empty()) {
        this->_resources.push_back(new T);
    }

    T* res = this->_resources.front();
    this->_resources.pop_front();
    return res;
}

template <class T>
void ObjectPool<T>::returnResource(T* resource)
{
    this->_resources.push_back(resource);
}

template class ObjectPool<Factory>;
