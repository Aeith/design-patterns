#ifndef STRATEGY_H
#define STRATEGY_H

#include <string>

class Strategy
{

public:
    Strategy() = default;
    virtual ~Strategy() = default;

    virtual std::string algorithm(std::string data) const = 0;

};

class Strategy1 : public Strategy
{

public:
    Strategy1() = default;
    virtual ~Strategy1() = default;

    virtual std::string algorithm(std::string data) const;

};

class Strategy2 : public Strategy
{

public:
    Strategy2() = default;
    virtual ~Strategy2() = default;

    virtual std::string algorithm(std::string data) const;

};

class Context
{

public:
    Context(Strategy* strategy) : _strategy(strategy) {}
    ~Context() = default;

    void setStrategy(Strategy* strategey) { _strategy = strategey; }

    std::string businessLogic() const;

private:
    Strategy* _strategy {nullptr};

};

#endif // STRATEGY_H
