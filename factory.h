#ifndef FACTORY_H
#define FACTORY_H

#include <string>

enum class ObjectEnum {
    DEFAULT_OBJECT,
    OBJECT_1,
    OBJECT_2,
    OBJECT_3
};

class Factory
{

public:
    // Ctor and Dtor
    explicit Factory() = default;
    virtual ~Factory() = default;

    std::string getName() const { return name; }

    /** Methods **/
    static Factory* create(ObjectEnum type);

protected:
    std::string name {""};
};

class Object1 : public Factory {

public:
    explicit Object1(std::string n = "") {name = n;}
    virtual ~Object1() = default;
};

class Object2 : public Factory {

public:
    explicit Object2(std::string n = "") {name = n;}
    virtual ~Object2() = default;
};

class Object3 : public Factory {

public:
    explicit Object3(std::string n = "") {name = n;}
    virtual ~Object3() = default;
};

#endif // FACTORY_H
