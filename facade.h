#ifndef FACADE_H
#define FACADE_H

#include <string>

// Optional
class System
{

public:
    System() = default;
    virtual ~System() = default;

    virtual bool prepare() = 0;
    virtual void execute() = 0;
    virtual std::string retrieve() const = 0;

protected:
    std::string _value {""};

};

class SubSystem1 : public System
{

public:
    SubSystem1() = default;
    virtual ~SubSystem1() = default;

    virtual bool prepare();
    virtual void execute();
    virtual std::string retrieve() const;

};

class SubSystem2 : public System
{

public:
    SubSystem2() = default;
    virtual ~SubSystem2() = default;

    virtual bool prepare();
    virtual void execute();
    virtual std::string retrieve() const;

};

class Facade
{

public:
    Facade() { _ss1 = new SubSystem1(); _ss2 = new SubSystem2(); };
    Facade(SubSystem1* ss1) : _ss1(ss1) { _ss2 = new SubSystem2(); };
    Facade(SubSystem2* ss2) : _ss2(ss2) { _ss1 = new SubSystem1(); };
    Facade(SubSystem1* ss1, SubSystem2* ss2) : _ss1(ss1), _ss2(ss2) {};
    virtual ~Facade() {
        delete _ss1; _ss1 = nullptr;
        delete _ss2; _ss2 = nullptr;
    }

    std::string execute() const;

private:
    SubSystem1* _ss1 {nullptr};
    SubSystem2* _ss2 {nullptr};

};

#endif // FACADE_H
