#ifndef VISITOR_H
#define VISITOR_H

#include <string>

class ConcreteComponentA;
class ConcreteComponentB;

class Visitor
{

public:
    Visitor() = default;
    virtual ~Visitor() = default;

    virtual std::string visitConcreteComponentA(const ConcreteComponentA* component) const = 0;
    virtual std::string visitConcreteComponentB(const ConcreteComponentB* component) const = 0;

};

class VisitorComponent
{

public:
    VisitorComponent() = default;
    virtual ~VisitorComponent() = default;

    virtual std::string accept(Visitor* visitor) const = 0;

};

class ConcreteComponentA : public VisitorComponent
{

public:
    ConcreteComponentA() = default;
    virtual ~ConcreteComponentA() = default;

    virtual std::string accept(Visitor* visitor) const;
    std::string functionA() const;

};

class ConcreteComponentB : public VisitorComponent
{

public:
    ConcreteComponentB() = default;
    virtual ~ConcreteComponentB() = default;

    virtual std::string accept(Visitor* visitor) const;
    std::string functionB() const;

};

class ConcreteVisitor1 : public Visitor
{

public:
    ConcreteVisitor1() = default;
    virtual ~ConcreteVisitor1() = default;

    virtual std::string visitConcreteComponentA(const ConcreteComponentA* component) const;
    virtual std::string visitConcreteComponentB(const ConcreteComponentB* component) const;

};

class ConcreteVisitor2 : public Visitor
{

public:
    ConcreteVisitor2() = default;
    virtual ~ConcreteVisitor2() = default;

    virtual std::string visitConcreteComponentA(const ConcreteComponentA* component) const;
    virtual std::string visitConcreteComponentB(const ConcreteComponentB* component) const;

};

#endif // VISITOR_H
