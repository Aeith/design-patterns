#ifndef COMMAND_H
#define COMMAND_H

#include <string>

class Command
{

public:
    Command() = default;
    virtual ~Command() = default;

    virtual std::string execute() const = 0;

};

class Receiver
{

public:
    Receiver() = default;
    virtual ~Receiver() = default;

    std::string action() const;
    std::string thing() const;

};

class ConcreteCommand : public Command
{

public:
    ConcreteCommand() = default;
    ConcreteCommand(Receiver* receiver) : _receiver(receiver) {}
    virtual ~ConcreteCommand() = default;

    virtual std::string execute() const;

private:
    Receiver* _receiver {nullptr};

};

#endif // COMMAND_H
