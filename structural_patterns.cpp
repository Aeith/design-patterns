#include "structural_patterns.h"

#include "adapter.h"
#include "bridge.h"
#include "composite.h"
#include "decorator.h"
#include "delegate.h"
#include "facade.h"
#include "flyweight.h"
#include "extension_object.h"
#include "proxy.h"
#include "twin.h"

void StructuralPatterns::showPatterns() const
{
    std::cout << "\n----------------------------------------------------" << std::endl;
    std::cout << "---------------- STRUCTURAL PATTERNS ---------------" << std::endl;
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Twin
    TwinA* tA = new TwinA();
    TwinB* tB = new TwinB(tA);
    tA->_twin = tB;
    showPattern("Twin", std::vector<void*>{
                                            tA,
                                            tB,
                                            tA->_twin,
                                            tB->_twin,
                        });
    delete tA, delete tB;
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Proxy
    RealSubject* rs = new RealSubject();
    Proxy* pr = new Proxy();
    showPattern("Proxy", std::vector<void*>{
                             rs,
                             pr
                         });
    rs->stuff(); pr->stuff();
    delete rs, delete pr;
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Extension Object
    ExtendedObject eo1; ExtensionObject eo2;
    showPattern("Extension Object", std::vector<std::string>{
                                        "Default - " + eo1.defaultMethod(),
                                        "Extended - " + eo2.extendedMethod(eo1)
                                    });
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Adapter
    Target tar; Adaptee ada; Adapter adp(ada);
    showPattern("Adapter", std::vector<std::string>{
                                "Target  - " + tar.targetMethod(11),
                                "Adaptee - " + ada.adapteeMethod(),
                                "Adapter - " + adp.targetMethod(11),
                           });
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Bridge
    ConcreteImplementation1* ci1 = new ConcreteImplementation1();
    ConcreteImplementation2* ci2 = new ConcreteImplementation2();
    Abstraction abs(ci1);
    ExtendedAbstraction extAbs(ci2);
    showPattern("Bridge", std::vector<std::string>{
                                                   ci1->operationImplementation(),
                                                   ci2->operationImplementation(),
                                                   abs.operationAbstraction(),
                                                   extAbs.operationAbstraction()
                           });
    delete ci1, delete ci2;
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Composite
    Composite* composite = new Composite();
    Composite* branch1 = new Composite();
    Composite* branch2 = new Composite();
    Leaf<int>* leaf1 = new Leaf<int>(12);
    Leaf<int>* leaf2 = new Leaf<int>(-666);
    Leaf<std::string>* leaf3 = new Leaf<std::string>("coucou");
    branch1->add(leaf1);
    branch1->add(leaf3);
    branch2->add(leaf2);
    composite->add(branch1);
    composite->add(branch2);
    showPattern("Composite", std::vector<std::string>{
                                                      "Branch 1 - " + branch1->execute(),
                                                      "Branch 2 - " + branch2->execute(),
                                                      "Tree - " + composite->execute()
                          });
    delete composite;
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Decorator
    ConcreteComponent* cc = new ConcreteComponent();
    ConcreteDecorator1* cd1 = new ConcreteDecorator1(cc);
    ConcreteDecorator2* cd2 = new ConcreteDecorator2(cd1);
    showPattern("Decorator", std::vector<std::string>{
                                                      cc->execute(),
                                                      cd1->execute(),
                                                      cd2->execute()
                             });
    delete cd2;
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Delegate
    Delegate* delegate = new Delegate();
    Delegee* delegee = new Delegee(delegate);
    showPattern("Delegate", std::vector<std::string>{
                                                    "Delegate - " + delegate->function(),
                                                    "Delegee - " + delegee->function(),
                             });
    delete delegee;
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Facade
    SubSystem1* ss1 = new SubSystem1();
    SubSystem2* ss2 = new SubSystem2();
    Facade facade(ss1, ss2);
    showPattern("Facade", std::vector<std::string>{
                                                    "SubSystem 1 - " + ss1->retrieve(),
                                                    "SubSystem 2 - " + ss2->retrieve(),
                                                    "Facade - " + facade.execute(),
                            });
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Flyweight
    std::cout << std::endl;
    FlyweightFactory* ffactory = new FlyweightFactory();
    ComplexObject co1(ffactory, 0, 0, 1280, 720, "T");
    ComplexObject co2(ffactory, 100, 100, 1280, 720, "T");
    ComplexObject co3(ffactory, 0, 100, 1280, 720, "CT");
    ComplexObject co4(ffactory, 100, 0, 1280, 720, "T");

    showPattern("Flyweight", std::vector<std::string>{
                                    co1.operation(),
                                    co2.operation(),
                                    co3.operation(),
                                    co4.operation(),
                          });
    delete ffactory;
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    std::cout << "\n------------ END OF STRUCTURAL PATTERNS ------------" << std::endl;
    std::cout << "----------------------------------------------------" << std::endl;
}
