#include "creational_patterns.h"

#include "abstract_factory.h"
#include "builder.h"
#include "dependency_injection.h"
#include "factory.h"
#include "lazy.h"
#include "multiton.h"
#include "object_pool.h"
#include "prototype.h"
#include "singleton.h"

void CreationalPatterns::showPatterns() const
{
    std::cout << "\n----------------------------------------------------" << std::endl;
    std::cout << "---------------- CREATIONAL PATTERNS ---------------" << std::endl;
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // SINGLETON
    showPattern("Singleton", std::vector<void*>{
                                 Singleton::Instance(),
                                 Singleton::Instance(),
                                 Singleton::Instance()
                             });
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // MULTITON
    showPattern("Multiton", std::vector<void*>{
                                Multiton::Instance(1),
                                Multiton::Instance(2),
                                Multiton::Instance(1)
                            });
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // FACTORY METHOD
    Factory* o1 = Factory::create(ObjectEnum::OBJECT_1);
    Factory* o2 = Factory::create(ObjectEnum::OBJECT_2);
    Factory* o3 = Factory::create(ObjectEnum::OBJECT_3);
    showPattern("Factory Method", std::vector<std::string>{
                                                           o1->getName(),
                                                           o2->getName(),
                                                           o3->getName()});
    delete o1, delete o2, delete o3;
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // OBJECT POOL
    auto pool = ObjectPool<Factory>::Instance();
    auto res1 = pool->getResource();
    showPattern("Object Pool", std::vector<Factory*>{
                                   res1,
                                   pool->getResource()
                               });
    pool->returnResource(res1);
    showPattern("Object Pool ; 1 Returned", std::vector<Factory*>{
                                                pool->getResource(),
                                                pool->getResource()
                                            });
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // PROTOTYPE
    ConcretePrototype1* c11 = new ConcretePrototype1(), *c12 = dynamic_cast<ConcretePrototype1*>(c11->clone());
    ConcretePrototype2* c21 = new ConcretePrototype2(), *c22 = dynamic_cast<ConcretePrototype2*>(c21->clone());
    if (c11 && c12 && c21 && c22) {
        showPattern("Prototype", std::vector<char>{
                                     c11->getAttribute(),
                                     c21->getAttribute(),
                                     c12->getAttribute(),
                                     c22->getAttribute()
                                 });
    }
    delete c11, delete c12, delete c21, delete c22;
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // BUILDER
    ConcreteBuilder1 cb11, cb12;
    cb11.BuildPart1(), cb11.BuildPart2(); cb11.BuildPart3();
    cb12.BuildPart1(); cb12.BuildPart3();
    ConcreteBuilder2 cb21, cb22;
    cb21.BuildPart1(); cb21.BuildPart2(); cb21.BuildPart3();
    cb22.BuildPart1(); cb22.BuildPart3();
    showPattern("Builder", std::vector<std::string>{
                               cb11.retrieveProduct()->toString(),
                               cb12.retrieveProduct()->toString(),
                               cb21.retrieveProduct()->toString(),
                               cb22.retrieveProduct()->toString(),
                           });
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // DEPENDENCY INJECTION
    Dependency1* dp1 = new Dependency1();
    Dependency* dp2 = new Dependency2();
    User *di1 = new User(dp1), *di2 = new User(dp2);
    showPattern("Dependency Injection", std::vector<std::string>{
                                            di1->work(),
                                            di2->work()
                                        });
    delete di1, delete di2;
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // LAZY INITIALIZATION
    Lazy<LazyTest> lazyTest; // Not instantiated yet
    std::cout << "\nLazy initialization example" << std::endl;
    std::cout << "First access since creation => initialization : " << lazyTest->getValue() << std::endl;
    std::cout << "Already initialized => access : " << (*lazyTest).getValue() << std::endl;
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // ABSTRACT FACTORY
    AbstractFactory *cf1 = new ConcreteFactory1(), *cf2 = new ConcreteFactory2();
    AbstractProductA *apA1 = cf1->createProductA(), *apA2 = cf2->createProductA();
    AbstractProductB *apB1 = cf1->createProductB(), *apB2 = cf2->createProductB();
    AbstractProductC *apC1 = cf1->createProductC(), *apC2 = cf2->createProductC();
    showPattern("Abstract Factory", std::vector<std::string>{
                                        apA1->productType(),
                                        apA2->productType(),
                                        apB1->productType(),
                                        apB2->productType(),
                                        apC1->productType(),
                                        apC2->productType()
                                    });
    delete apA1, delete apA2, delete apB1, delete apB2, delete apC1, delete apC2, delete cf1, delete cf2;
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    std::cout << "\n------------ END OF CREATIONAL PATTERNS ------------" << std::endl;
    std::cout << "----------------------------------------------------" << std::endl;
}
