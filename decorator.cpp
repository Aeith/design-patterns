#include "decorator.h"

std::string Decorator::execute() const
{
    return _wrappee->execute();
}

std::string ConcreteComponent::execute() const
{
    return "Concrete Component";
}

std::string ConcreteDecorator1::execute() const
{
    return Decorator::execute() + extra1();
}

std::string ConcreteDecorator1::extra1() const
{
    return " | Extra 1";
}

std::string ConcreteDecorator2::execute() const
{
    return Decorator::execute() + extra2();
}

std::string ConcreteDecorator2::extra2() const
{
    return " | Extra 2";
}
