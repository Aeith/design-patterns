#ifndef SINGLETON_H
#define SINGLETON_H

/**
 * What's the point ?
 * Require a single instance ? Call it once.
 * Require shared methods accross the code ? What about a static helper class ?
 **/

#include <mutex>

class Singleton {

private:
    // Ctor Dtor
    Singleton() = default;
    ~Singleton() { delete _instance; }

    /** Attributes **/
    static Singleton * _instance;
    static std::mutex _mutex;

public:
    // Assign / Copy
    Singleton(Singleton & other) = delete;
    void operator=(const Singleton &) = delete;

    /** Methods **/
    static Singleton * Instance();
};

#endif // SINGLETON_H
