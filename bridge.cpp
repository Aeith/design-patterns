#include "bridge.h"

std::string ConcreteImplementation1::operationImplementation() const
{
    return "Concrete Implementation 1";
}

std::string ConcreteImplementation2::operationImplementation() const
{
    return "Concrete Implementation 2";
}

std::string Abstraction::operationAbstraction() const
{
    return "Abstraction : " + _implementation->operationImplementation();
}

std::string ExtendedAbstraction::operationAbstraction() const
{
    return "Extended Abstraction : " + _implementation->operationImplementation();
}
