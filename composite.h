#ifndef COMPOSITE_H
#define COMPOSITE_H

#include <string>

class Component
{

public:
    // Ctor and dtor
    Component() = default;
    virtual ~Component() = default;

    /** Methods **/
    virtual std::string execute() const = 0;

};

template <class T>
class Leaf : public Component
{

public:
    // Ctor and dtor
    Leaf(T val) : _value(val) {}
    virtual ~Leaf() = default;

    /** Methods **/
    virtual std::string execute() const override {
        return _value;
    }

private:
    T _value;

};

template <>
class Leaf<int> : public Component
{

public:
    // Ctor and dtor
    Leaf(int val) : _value(val) {}
    virtual ~Leaf() = default;

    /** Methods **/
    virtual std::string execute() const override {
        return std::to_string(_value);
    }

private:
    int _value;

};

class Composite : public Component
{

public:
    // Ctor and dtor
    Composite() = default;
    virtual ~Composite() {
        for (auto child : _children) {
            delete child; child = nullptr;
        }
    }

    /** Accessors **/
    void add(Component* c);
    void remove(Component* c);
    std::vector<Component*> getChildren() const { return _children; }

    /** Methods **/
    virtual std::string execute() const override;

private:
    /** Attributes **/
    std::vector<Component*> _children = {};

};

#endif // COMPOSITE_H
