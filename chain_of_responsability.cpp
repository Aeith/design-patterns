#include "chain_of_responsability.h"

Handler* AbstractHandler::setNext(Handler* next)
{
    if (next)
        _next = next;

    return _next;
}

std::string AbstractHandler::handle(std::string request)
{
    if (_next)
        return this->_next->handle(request);

    return "";
}


std::string Handler1::handle(std::string request)
{
    if (request == "condition1")
        return "I, Handler 1, will handle this request : condition1";

    return AbstractHandler::handle(request);
}

std::string Handler2::handle(std::string request)
{
    if (request == "condition2")
        return "I, Handler 2, will handle this request : condition2";

    return AbstractHandler::handle(request);
}
