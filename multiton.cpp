#include "multiton.h"

std::map<unsigned int, Multiton *> Multiton::_instances;
std::mutex Multiton::_mutex;

Multiton * Multiton::Instance(unsigned int key) {
    std::lock_guard<std::mutex> lock(_mutex);

    if (Multiton::_instances.find(key) == Multiton::_instances.end()) {
        Multiton::_instances[key] = new Multiton();
    }

    return Multiton::_instances[key];
}
