#ifndef PROTOTYPE_H
#define PROTOTYPE_H

class Prototype
{

public:
    // Ctor and dtor
    explicit Prototype() = default;
    virtual ~Prototype() = default;

    /** Accessors **/
    virtual char getAttribute() const { return att1; }

    /** Methods **/
    virtual Prototype* clone() = 0;

protected:
    /** Attributes **/
    char att1 {' '};
};

class ConcretePrototype1 : public Prototype {

public:
    // Ctor and dtor
    explicit ConcretePrototype1() = default;
    virtual ~ConcretePrototype1() = default;

    // Copy ctor
    ConcretePrototype1(const ConcretePrototype1& o) {
        this->att1 = o.att1;
        this->att2 = o.att2;
    }

    /** Accessors **/
    virtual char getAttribute() const override { return att2; }

    /** Methods **/
    virtual Prototype* clone() override { return new ConcretePrototype1(*this); }

private:
    char att2 {'b'};

};

class ConcretePrototype2 : public Prototype {

public:
    // Ctor and dtor
    explicit ConcretePrototype2() = default;
    virtual ~ConcretePrototype2() = default;

    // Copy ctor
    ConcretePrototype2(const ConcretePrototype2& o) {
        this->att1 = o.att1;
        this->att3 = o.att3;
    }

    /** Accessors **/
    virtual char getAttribute() const override { return att3; }

    /** Methods **/
    virtual Prototype* clone() override { return new ConcretePrototype2(*this); }

private:
    char att3 {'^'};

};

#endif // PROTOTYPE_H
