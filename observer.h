#ifndef OBSERVER_H
#define OBSERVER_H

#include <iostream>
#include <vector>
#include <string>

class Observer;

class Subject
{

public:
    Subject() = default;
    virtual ~Subject() = default;

    virtual void attach(Observer* obs) = 0;
    virtual void detach(Observer* obs) = 0;
    virtual void notify() = 0;

    std::string getName() const { return _name; }
    void setName(const std::string& name) {
        _name = name;
        notify();
    }

private:
    std::string _name {""};

};

class Observer
{

public:
    Observer(Subject* subject);
    virtual ~Observer();

    virtual void update(Subject* s) = 0;

protected:
    Subject* _subject;

};

class ConcreteObserver : public Observer
{

public:
    ConcreteObserver(Subject* subject) : Observer(subject) {}
    virtual ~ConcreteObserver() = default;

    virtual void update(Subject* s) override;

};

class ConcreteSubject : public Subject
{

public:
    ConcreteSubject() = default;
    virtual ~ConcreteSubject() = default;

    virtual void attach(Observer* obs) override
    {
        _observers.push_back(obs);
    }

    virtual void detach(Observer* obs) override
    {
        auto find = std::find_if(
            _observers.begin(),
            _observers.end(),
            [&obs](const auto& _observer) { return obs == _observer; });
        _observers.erase(find);
    }

    virtual void notify() override
    {
        for (auto& observer : _observers)
        {
            observer->update(this);
        }
    }

private:
    std::vector<Observer*> _observers = {};

};

#endif // OBSERVER_H
