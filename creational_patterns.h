#ifndef CREATIONAL_PATTERNS_H
#define CREATIONAL_PATTERNS_H

#include "abstract_pattern.h"

class CreationalPatterns : public AbstractPattern
{

public:
    CreationalPatterns() = default;
    ~CreationalPatterns() = default;

    void showPatterns() const;
};

#endif // CREATIONAL_PATTERNS_H
