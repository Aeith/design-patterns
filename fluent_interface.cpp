#include "fluent_interface.h"

FluentInterface& FluentInterface::PosX(int x)
{
    _x = x;
    return (*this);
}

FluentInterface& FluentInterface::PosY(int y)
{
    _y = y;
    return (*this);
}

FluentInterface& FluentInterface::Pos(int x, int y)
{
    return PosX(x).PosY(y);
}

std::unique_ptr<FluentInterface> FluentInterface::Build()
{
    return std::make_unique<FluentInterface>(*this);
}
