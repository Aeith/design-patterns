#ifndef TWIN_H
#define TWIN_H

/**
 * Super tight coupling
 * Circular dependencies
 * Delete hell
 * Don't.
 */

class TwinB;

class TwinA
{

public:
    TwinA() = default;
    TwinA(TwinB* b) : _twin(b) {};
    ~TwinA() = default;

    TwinB* _twin {nullptr};

};

class TwinB
{

public:
    TwinB() = default;
    TwinB(TwinA* a) : _twin(a) {};
    ~TwinB() = default;

    TwinA* _twin {nullptr};

};

#endif // TWIN_H
