#include "builder.h"

void ConcreteBuilder1::BuildPart1() {
    _product = std::make_unique<Product>();
    _product->setName("Default 1");
    _product->setValue(666);
}

void ConcreteBuilder1::BuildPart2() {
    _product->setValue(_product->getValue()*2);
}

void ConcreteBuilder1::BuildPart3() {
    _product->getValue() < 1000 ? _product->setName("Below")
                                : _product->setName("Above");
}

std::unique_ptr<Product> ConcreteBuilder1::retrieveProduct() {
    return std::move(_product);
}

void ConcreteBuilder2::BuildPart1() {
    _product = std::make_unique<Product>();
    _product->setName("Default 2");
}

void ConcreteBuilder2::BuildPart2() {
    _product->setName("Builder");
}

void ConcreteBuilder2::BuildPart3() {
    int val = 0;
    for (size_t i = 0; i < _product->getName().size(); i++) {
        val += static_cast<int>(_product->getName()[i]);
    }
    _product->setValue(val);
}

// No longer owns the product
std::unique_ptr<Product> ConcreteBuilder2::retrieveProduct() {
    return std::move(_product);
}
