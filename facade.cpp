#include "facade.h"

bool SubSystem1::prepare()
{
    return true;
}

void SubSystem1::execute()
{
    // lot of things
    _value = "done";
}

std::string SubSystem1::retrieve() const
{
    return _value;
}

bool SubSystem2::prepare()
{
    return _value.empty();
}

void SubSystem2::execute()
{
    // lot of things
    _value = "wip";
}

std::string SubSystem2::retrieve() const
{
    return _value;
}

std::string Facade::execute() const
{
    std::string res = "";
    if (_ss1 && _ss1->prepare())
    {
        _ss1->execute();
        if (_ss1->retrieve() == "done")
            res = "ok";
    }

    if (_ss2 && _ss2->prepare())
    {
        _ss2->execute();
        if (_ss2->retrieve() != "done")
            res += " but still work in progress";
    }

    return res;
}
