#include "factory.h"

Factory* Factory::create(ObjectEnum type){
    switch (type) {
    case ObjectEnum::OBJECT_1:
        return new Object1("object1");
    case ObjectEnum::OBJECT_2:
        return new Object2("object2");
    case ObjectEnum::OBJECT_3:
        return new Object3("object3");
    default:
        return nullptr;
    }
};
