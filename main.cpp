#include "patterns.h"

int main([[maybe_unused]] int argc,[[maybe_unused]] char** argv)
{
    Patterns patterns;
    patterns.testAll();

    return 0;
}
