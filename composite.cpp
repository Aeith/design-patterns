#include "composite.h"

void Composite::add(Component* c)
{
    _children.push_back(c);
}

void Composite::remove(Component* c)
{
    for (auto it = _children.begin(); it != _children.end(); ++it) {
        if (c == (*it)) {
            _children.erase(it);
            break;
        }
    }
}

std::string Composite::execute() const
{
    std::string res = "";
    for (size_t i = 0; i < _children.size(); i++) {
        res += _children[i]->execute();
        if (i < _children.size() - 1)
            res += ",";
    }
    return res;
}
