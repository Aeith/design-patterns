#ifndef OBJECT_POOL_H
#define OBJECT_POOL_H

/**
 * Uses singleton so same issues...
 * No multithreading allowed
 * Still instanciate stuff, requires lot of memory
 **/

#include <list>

template <class T>
class ObjectPool
{

public:
    // Assign / Copy
    ObjectPool(ObjectPool & other) = delete;
    void operator=(const ObjectPool &) = delete;

    T* getResource();
    void returnResource(T* resource);

    /** Methods **/
    static ObjectPool * Instance();

private:
    ObjectPool() = default;
    ~ObjectPool()  { delete _instance; }

    std::list<T*> _resources = {};

    /** Attributes **/
    static ObjectPool * _instance;

};

#endif // OBJECT_POOL_H
