#include "memento.h"

void Caretaker::save()
{
    _saves.push_back(_originator->save());
}

void Caretaker::undo()
{
    if (_saves.empty())
        return;

    Memento* memento = _saves.back();
    _saves.pop_back();

    try {
        _originator->restore(memento);
    } catch (...) {
        undo();
    }
}

void Caretaker::list()
{
    std::cout << "Current state" << std::endl;
    for (auto save : _saves)
    {
        auto s = save->getState();
        std::cout << s.name << " is level " << s.level << " and ranked as : " << s.rank << std::endl;
    }
}

void Originator::advance()
{
    _state.level++;
}

void Originator::levelUp()
{
    _state.rank = State::Rank(int(_state.rank) + 1);
}
