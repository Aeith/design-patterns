#include "extension_object.h"

std::string ExtendedObject::defaultMethod() const
{
    return "value : " + std::to_string(_value) + " | flag : " + ((_flag) ? "true" : "false");
}

std::string ExtensionObject::extendedMethod(ExtendedObject o) const
{
    return std::to_string(o._value * 5);
}
