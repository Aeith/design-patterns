#ifndef MEDIATOR_H
#define MEDIATOR_H

#include <iostream>
#include <string>

class BaseComponent;

class Mediator
{

public:
    Mediator() = default;
    virtual ~Mediator() = default;

    virtual void notify(BaseComponent* sender, std::string event) const = 0;

};

class BaseComponent
{

public:
    BaseComponent() = default;
    BaseComponent(Mediator* mediator) : _mediator(mediator) {}
    virtual ~BaseComponent() = default;

    void setMediator(Mediator* mediator) { _mediator = mediator; }

protected:
    Mediator* _mediator {nullptr};

};

class ButtonComponent : public BaseComponent
{

public:
    ButtonComponent() = default;
    ButtonComponent(Mediator* mediator) : BaseComponent(mediator) {}
    ~ButtonComponent() = default;

    bool getEnabled() const { return _enabled; }
    void setEnabled(bool enabled) { _enabled = enabled; }

    void clicked();

private:
    bool _enabled {false};

};

class TextComponent : public BaseComponent
{

public:
    TextComponent() = default;
    TextComponent(Mediator* mediator) : BaseComponent(mediator) {}
    ~TextComponent() = default;

    std::string getText() const { return _text; }
    void setText(const std::string& text) { _text = text; }

    void textChanged();

private:
    std::string _text {""};

};

class UIMediator : public Mediator
{

public:
    UIMediator(ButtonComponent* bc, TextComponent* tc) : _button(bc), _text(tc) {
        _button->setMediator(this);
        _text->setMediator(this);
    }
    virtual ~UIMediator() {
        delete _button;
        delete _text;
    };

    bool checkValidity(const std::string& text) const { return !text.empty(); }

    virtual void notify(BaseComponent* sender, std::string event) const;

private:
    ButtonComponent* _button {nullptr};
    TextComponent* _text {nullptr};

};

#endif // MEDIATOR_H
