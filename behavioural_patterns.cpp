#include "behavioural_patterns.h"

#include "chain_of_responsability.h"
#include "command.h"
#include "fluent_interface.h"
#include "interpreter.h"
#include "mediator.h"
#include "memento.h"
#include "observer.h"
#include "servant.h"
#include "specification.h"
#include "strategy.h"
#include "visitor.h"

void BehaviouralPatterns::showPatterns() const
{
    std::cout << "\n-----------------------------------------------------" << std::endl;
    std::cout << "---------------- BEHAVIOURAL PATTERNS ---------------" << std::endl;
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Chain or Responsability
    Handler1* h1 = new Handler1();
    Handler2* h2 = new Handler2();
    h1->setNext(h2);

    showPattern("Chain of Responsability", std::vector<std::string>{
                                                "H1 | C1 - " + h1->handle("condition1"),
                                                "H2 | C1 - " + h2->handle("condition1"),
                                                "H1 | C2 - " + h1->handle("condition2"),
                                                "H2 | C2 - " + h2->handle("condition2"),
                                                "H1 | C3 - " + h1->handle("condition3"),
                                                "H2 | C3 - " + h2->handle("condition3"),
                         });

    delete h1;
    delete h2;
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Fluent Interface
    FluentInterface fi;

    std::cout << "\nFluent Interface : " << fi.getX() << "," << fi.getY() << std::endl;

    fi
        .PosX(12)
        .PosY(9)
        .Build();

    std::cout << "Fluent Interface : " << fi.getX() << "," << fi.getY() << std::endl;
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Mediator
    ButtonComponent* button = new ButtonComponent();
    TextComponent* text = new TextComponent();
    UIMediator* mediator = new UIMediator(button, text);

    std::cout << "\nMediator : " << std::endl;

    button->clicked();
    text->textChanged();

    delete mediator;
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Servant
    MoveServant* moveServant = new MoveServant();
    Ellipse* ellipse = new Ellipse();
    Rectangle* rectangle = new Rectangle();

    std::cout << "\nServant Ellipse : " << ellipse->getPosition();
    moveServant->moveTo(ellipse, Position(10, 100));
    std::cout << " moved to x=10 and y=100 : " << ellipse->getPosition() << std::endl;

    std::cout << "Servant Rectangle : " << rectangle->getPosition();
    moveServant->moveBy(rectangle, 5, 7);
    std::cout << " moved by 5x and 7y : " << rectangle->getPosition() << std::endl;

    delete moveServant;
    delete ellipse;
    delete rectangle;
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Command
    Receiver* receiver = new Receiver();
    ConcreteCommand* command = new ConcreteCommand(receiver);
    showPattern("Command", std::vector<std::string>{
                                    command->execute()
                           });
    delete command;
    delete receiver;
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Strategy
    Strategy1* strat1 = new Strategy1();
    Strategy2* strat2 = new Strategy2();
    Context* context = new Context(strat1);

    std::cout << "\nStrategy : strat 1 - " << context->businessLogic() << std::endl;
    context->setStrategy(strat2);
    std::cout << "Strategy : strat 2 - " << context->businessLogic() << std::endl;

    delete context;
    delete strat1;
    delete strat2;
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Visitor
    ConcreteVisitor1* cvisitor1 = new ConcreteVisitor1();
    ConcreteVisitor2* cvisitor2 = new ConcreteVisitor2();
    ConcreteComponentA* ccomponentA = new ConcreteComponentA();
    ConcreteComponentB* ccomponentB = new ConcreteComponentB();

    showPattern("Visitor", std::vector<std::string>{
                               ccomponentA->accept(cvisitor1),
                               ccomponentA->accept(cvisitor2),
                               ccomponentB->accept(cvisitor1),
                               ccomponentB->accept(cvisitor2),
                           });

    delete cvisitor1;
    delete cvisitor2;
    delete ccomponentA;
    delete ccomponentB;
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Interpreter
    CalculatorInterpreter interpreter;
    showPattern("Interpreter", std::vector<int>{
                                    interpreter.interpret("13 * 4"),                // = 52
                                    interpreter.interpret("(7+ 2)"),                // = 9
                                    interpreter.interpret("18 / (7+ 2)"),           // = 2
                                    interpreter.interpret("13 * 4-18"),             // = 34
                                    interpreter.interpret("4-18 / (7+ 2)"),         // = 2
                                    interpreter.interpret("13 * 4-18 / (7+ 2)"),    // = 50
                           });
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Memento
    Originator* originator = new Originator("Aeith");
    Caretaker caretaker(originator);
    std::cout << "\nMemento advancing" << std::endl;
    caretaker.save();
    originator->advance();
    caretaker.save();
    originator->levelUp();
    caretaker.save();
    originator->advance();
    caretaker.save();
    originator->advance();
    caretaker.save();
    originator->levelUp();
    caretaker.save();

    caretaker.list();

    std::cout << "Undoing two actions" << std::endl;
    caretaker.undo();
    caretaker.undo();

    caretaker.list();

    delete originator;
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Observer
    ConcreteSubject* subject = new ConcreteSubject();
    ConcreteObserver* observer1 = new ConcreteObserver(subject);
    ConcreteObserver* observer2 = new ConcreteObserver(subject);

    std::cout << "\nObserver" << std::endl;
    subject->setName("test");
    subject->setName("new");
    subject->setName("end");

    delete observer1; delete observer2;
    delete subject;
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Specification
    [[maybe_unused]]auto overDue = new OverDueSpecification();
    [[maybe_unused]]auto noticeSent = new NoticeSentSpecification();
    [[maybe_unused]]auto inCollection = new InCollectionSpecification();

    /*overDue->And(noticeSent)->And(inCollection->Not());

    I have not FUCKING CLUE how I'm supposed to implement this shit.*/
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    std::cout << "\n------------ END OF BEHAVIOURAL PATTERNS ------------" << std::endl;
    std::cout << "-----------------------------------------------------" << std::endl;
}
