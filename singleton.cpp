#include "singleton.h"

Singleton * Singleton::_instance{nullptr};
std::mutex Singleton::_mutex;

Singleton * Singleton::Instance() {
    std::lock_guard<std::mutex> lock(_mutex);

    if (Singleton::_instance == nullptr)
        _instance = new Singleton();

    return _instance;
}
