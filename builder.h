#ifndef BUILDER_H
#define BUILDER_H

#include <string>

class Builder
{

public:
    // Ctor and dtor
    explicit Builder() = default;
    virtual ~Builder() = default;

    /** Methods **/
    virtual void BuildPart1() = 0;
    virtual void BuildPart2() = 0;
    virtual void BuildPart3() = 0;

};

class Product
{

public:
    //Ctor and dtor
    explicit Product() = default;
    virtual ~Product() = default;

    /** Acccessors **/
    std::string getName() const { return _name; }
    int getValue() const { return _value; }
    void setName(const std::string& name) { _name = name; }
    void setValue(const int value) { _value = value; }

    /** Methods **/
    std::string toString() { return _name + " = " + std::to_string(_value); }

private:
    std::string _name {""};
    int _value {0};

};

class ConcreteBuilder1 : public Builder
{

public:
    // Ctor and dtor
    explicit ConcreteBuilder1() = default;
    virtual ~ConcreteBuilder1() = default;

    /** Methods **/
    virtual void BuildPart1() override;
    virtual void BuildPart2() override;
    virtual void BuildPart3() override;
    std::unique_ptr<Product> retrieveProduct();

private:
    std::unique_ptr<Product> _product {nullptr};

};


class ConcreteBuilder2 : public Builder
{

public:
    // Ctor and dtor
    explicit ConcreteBuilder2() = default;
    virtual ~ConcreteBuilder2() = default;

    /** Methods **/
    virtual void BuildPart1() override;
    virtual void BuildPart2() override;
    virtual void BuildPart3() override;
    std::unique_ptr<Product> retrieveProduct();

private:
    std::unique_ptr<Product> _product {nullptr};
};

#endif // BUILDER_H
