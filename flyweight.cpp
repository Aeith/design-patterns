#include "flyweight.h"

long long SharedObject::toUUID() const
{
    std::string concat = "";
    for (size_t i = 0; i < _name.size(); i++) {
        concat += std::to_string(_name[i]);
    }

    return std::stoll(concat);
}

Flyweight* FlyweightFactory::get(SharedObject* shObject)
{
    long long key = getKey(shObject);
    if (key == 0)
        return nullptr;

    if (_flyweights.find(key) == _flyweights.end()) {
        std::cout << "Generating a new flyweight for shared object " << shObject->getName() << std::endl;
        _flyweights.insert(std::make_pair(key, new Flyweight(shObject)));
    } else
        std::cout << "Already existing shared object " << shObject->getName() << std::endl;

    return _flyweights[key];
}

void FlyweightFactory::list() const
{
    for (auto pair : _flyweights)
    {
        std::cout << pair.second->getObject()->getName() << std::endl;
    }
}

int FlyweightFactory::getKey(SharedObject* shObject) const
{
    if (shObject)
        return shObject->toUUID();

    return 0;
}
