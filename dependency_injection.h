#ifndef DEPENDENCYINJECTION_H
#define DEPENDENCYINJECTION_H

#include <string>

class Dependency
{

public:
    // Ctor and dtor
    explicit Dependency() = default;
    virtual ~Dependency() = default;

    /** Methods **/
    virtual std::string working() = 0;

};

class Dependency1 : public Dependency {

public:
    // Ctor and dtor
    explicit Dependency1() = default;
    virtual ~Dependency1() = default;

    std::string working() { return "doing something..."; }

};

class Dependency2 : public Dependency {

public:
    // Ctor and dtor
    explicit Dependency2() = default;
    virtual ~Dependency2() = default;

    std::string working() { return "doing something else..."; }

};

class User {

public:
    // Ctor and dtor
    explicit User(Dependency* dep) : _dependency(dep) {};
    virtual ~User() { delete _dependency; _dependency = nullptr; };

    std::string work() {
        if (_dependency)
            return _dependency->working();

        return "";
    }

private:
    /** Attributes **/
    Dependency* _dependency {nullptr};

};

#endif // DEPENDENCYINJECTION_H
