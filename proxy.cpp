#include "proxy.h"

void RealSubject::stuff()
{
    std::cout << "Real Subject : doing stuff..." << std::endl;
}

void Proxy::stuff()
{
    preStuff();
    _subject.stuff();
    postStuff();
}

void Proxy::preStuff()
{
    std::cout << "Proxy : preparing stuff" << std::endl;
}

void Proxy::postStuff()
{
    std::cout << "Proxy : cleaning stuff" << std::endl;
}
