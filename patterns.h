#ifndef PATTERNS_H
#define PATTERNS_H

#include "behavioural_patterns.h"
#include "creational_patterns.h"
#include "structural_patterns.h"
#include "concurrency_patterns.h"

class Patterns
{

public:
    // Ctor and dtor
    Patterns() = default;
    virtual ~Patterns() = default;

    void testAll() const;

private:
    /** Attributes **/
    BehaviouralPatterns _behaviourals;
    CreationalPatterns _creationals;
    StructuralPatterns _structurals;
    ConcurrencyPatterns _concurrency;

};

#endif // PATTERNS_H
