#ifndef SPECIFICATION_H
#define SPECIFICATION_H

/**
 * Funny to have a boolean POV for business requirements
 * BUT WAYYYYYY to complicated, too much trouble, too little usage
 **/

#include <string>

template <class T>
class Specification
{

public:
    Specification() = default;
    virtual ~Specification() = default;

    virtual Specification<T>* Not() const = 0;
    virtual Specification<T>* And(const Specification<T>& other) const = 0;
    virtual Specification<T>* Or(const Specification<T>& other) const = 0;
    virtual Specification<T>* AndNot(const Specification<T>& other) const = 0;
    virtual Specification<T>* OrNot(const Specification<T>& other) const = 0;
    virtual bool IsSatisfiedBy(T object) const = 0;

};

template <class T>
class CompositeSpecification : public Specification<T>
{

public:
    CompositeSpecification() = default;
    virtual ~CompositeSpecification() = default;

    virtual Specification<T>* Not() const override;
    virtual Specification<T>* And(const Specification<T>& other) const override;
    virtual Specification<T>* Or(const Specification<T>& other) const override;
    virtual Specification<T>* AndNot(const Specification<T>& other) const override;
    virtual Specification<T>* OrNot(const Specification<T>& other) const override;
};

template <class T>
class NotSpecification final : public CompositeSpecification<T>
{

public:
    NotSpecification(const Specification<T>& specification) : _other(specification) { }

    virtual bool IsSatisfiedBy(T Candidate) const override
    {
        return !_other.IsSatisfiedBy(Candidate);
    }

private:
    const Specification<T>& _other;

};

template <class T>
class AndSpecification final : public CompositeSpecification<T>
{

public:
    AndSpecification(const Specification<T>& inLeft, const Specification<T>& inRight) : _left(inLeft), _right(inRight) { }

    virtual bool IsSatisfiedBy(T Candidate) const override
    {
        return _left.IsSatisfiedBy(Candidate) && _right.IsSatisfiedBy(Candidate);
    }

private:
    const Specification<T>& _left;
    const Specification<T>& _right;

};

template <class T>
class OrSpecification final : public CompositeSpecification<T>
{

public:
    OrSpecification(const Specification<T>& inLeft, const Specification<T>& inRight) : _left(inLeft), _right(inRight) { }

    virtual bool IsSatisfiedBy(T Candidate) const override
    {
        return _left.IsSatisfiedBy(Candidate) || _right.IsSatisfiedBy(Candidate);
    }

private:
    const Specification<T>& _left;
    const Specification<T>& _right;

};

template <class T>
class AndNotSpecification final : public CompositeSpecification<T>
{

public:
    AndNotSpecification(const Specification<T>& inLeft, const Specification<T>& inRight) : _left(inLeft), _right(inRight) { }

    virtual bool IsSatisfiedBy(T Candidate) const override
    {
        return _left.IsSatisfiedBy(Candidate) && !_right.IsSatisfiedBy(Candidate);
    }

private:
    const Specification<T>& _left;
    const Specification<T>& _right;

};

template <class T>
class OrNotSpecification final : public CompositeSpecification<T>
{

public:
    OrNotSpecification(const Specification<T>& inLeft, const Specification<T>& inRight) : _left(inLeft), _right(inRight) { }

    virtual bool IsSatisfiedBy(T Candidate) const override
    {
        return _left.IsSatisfiedBy(Candidate) || !_right.IsSatisfiedBy(Candidate);
    }

private:
    const Specification<T>& _left;
    const Specification<T>& _right;

};

template <class T>
Specification<T>* CompositeSpecification<T>::And(const Specification<T>& Other) const
{
    return new AndSpecification<T>(*this, Other);
}
template <class T>
Specification<T>* CompositeSpecification<T>::AndNot(const Specification<T>& Other) const
{
    return new AndNotSpecification<T>(*this, Other);
}

template <class T>
Specification<T>* CompositeSpecification<T>::Or(const Specification<T>& Other) const
{
    return new OrSpecification<T>(*this, Other);
}

template <class T>
Specification<T>* CompositeSpecification<T>::OrNot(const Specification<T>& Other) const
{
    return new OrNotSpecification<T>(*this, Other);
}

template <class T>
Specification<T>* CompositeSpecification<T>::Not() const
{
    return new NotSpecification<T>(*this);
}

class Invoice {

public:
    Invoice() = default;
    Invoice(int overDue) : _overDue(overDue) {}
    ~Invoice() = default;

    int getOverDue() const { return _overDue; }

private:
    int _overDue {100};

};

class OverDueSpecification final : public CompositeSpecification<Invoice> {

public:
    OverDueSpecification() = default;
    virtual ~OverDueSpecification() = default;

    virtual bool IsSatisfiedBy(Invoice object) const override;
};

class Notice {

public:
    Notice() = default;
    Notice(int count) : _sentCount(count) {}
    ~Notice() = default;

    int getSentCount() const { return _sentCount; }

private:
    int _sentCount {0};

};

class NoticeSentSpecification final : public CompositeSpecification<Notice> {

public:
    NoticeSentSpecification() = default;
    virtual ~NoticeSentSpecification() = default;

    virtual bool IsSatisfiedBy(Notice object) const override;
};

class Agency {

public:
    Agency() = default;
    Agency(bool isSent) : _isSent(isSent) {};
    ~Agency() = default;

    bool isSent() const { return _isSent; }

private:
    bool _isSent {false};

};

class InCollectionSpecification final : public CompositeSpecification<Agency> {

public:
    InCollectionSpecification() = default;
    virtual ~InCollectionSpecification() = default;

    virtual bool IsSatisfiedBy(Agency object) const override;

};

#endif // SPECIFICATION_H
