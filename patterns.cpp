#include "patterns.h"

void Patterns::testAll() const
{
    _creationals.showPatterns();
    _structurals.showPatterns();
    _behaviourals.showPatterns();
    _concurrency.showPatterns();
}
