#ifndef FLUENT_INTERFACE_H
#define FLUENT_INTERFACE_H

/**
 * Let Java do this shit.
 * Logging becomes complicated, debugging as well.
 * RIP Memory and readability. Everything returns the same thing, semantic is lost.
 **/

#include <memory>

class FluentInterface
{

public:
    FluentInterface() = default;
    ~FluentInterface() = default;

    FluentInterface& PosX(int x);
    FluentInterface& PosY(int y);
    FluentInterface& Pos(int x, int y);
    std::unique_ptr<FluentInterface> Build();

    int getX() const { return _x; }
    int getY() const { return _y; }

private:
    int _x {0};
    int _y {0};

};

#endif // FLUENT_INTERFACE_H
