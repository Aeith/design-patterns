#include "command.h"

std::string Receiver::action() const
{
    return "(Receiver action)";
}

std::string Receiver::thing() const
{
    return "(Receiver thing)";
}

std::string ConcreteCommand::execute() const
{
    return "Command execution -- " + _receiver->action() + " and " + _receiver->thing();
}
