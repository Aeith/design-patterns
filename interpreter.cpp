#include "interpreter.h"

#include <iostream>

std::unordered_map<Type, int> priorities = {
    {Type::None, 999},
    {Type::Add, 100},
    {Type::Substract, 99},
    {Type::Multiply, 10},
    {Type::Divide, 9},
    {Type::Group, 1},
    {Type::Value, 0}
};
std::unordered_map<Type, char> operators = {
    {Type::Add, '+'},
    {Type::Substract, '-'},
    {Type::Multiply, '*'},
    {Type::Divide, '/'}
};

std::pair<std::string, std::string> split(const std::string& s, char c, bool next = true)
{
    bool foundFlag = false;
    std::pair<std::string, std::string> res;

    for (auto it = s.begin(); it != s.end(); ++it)
    {
        if (*it == c) {
            foundFlag = true;

            if (next)
                continue;
        }

        (foundFlag) ? res.second += *it
                    : res.first += *it;
    }

    return res;
}

std::string removeGroup(const std::string& s)
{
    std::string local = s;
    size_t openGroup = s.find("(");

    if (openGroup != std::string::npos)
        local.replace(openGroup, 1, "");

    size_t closeGroup = local.find(")");

    if (closeGroup != std::string::npos)
        local.replace(closeGroup, 1, "");

    return local;
}

int CalculatorInterpreter::interpret(const std::string& context)
{
    std::string local = context;
    local.erase(remove_if(local.begin(), local.end(), isspace), local.end());

    // Reset expression between interpretations
    delete _expression; _expression = nullptr;

    /** Prepare expessions **/
    std::vector<Interpreter*> groupsAndOperators = generateInterpreters(context);
    _expression = getHighestPriority(groupsAndOperators); // Highest prio = last to evaluate

    return _expression->interpret(local);
}

int CalculatorInterpreter::getPriority() const
{
    return 0;
}

std::vector<Interpreter*> CalculatorInterpreter::generateInterpreters(const std::string& context, Type type)
{
    std::vector<Interpreter*> interpretors = {};

    size_t startGroup = context.find('('), endGroup = context.find(')');

    if (startGroup != std::string::npos && endGroup != std::string::npos)
    {
        std::pair<std::string, std::string> before = split(context, '(');
        std::pair<std::string, std::string> after = split(before.second, ')');

        if (!before.first.empty()) {
            auto vbefore = generateInterpreters(before.first);
            interpretors.insert(interpretors.end(), vbefore.begin(), vbefore.end());
        }

        if (!after.second.empty()) {
            auto vafter = generateInterpreters(after.second);
            interpretors.insert(interpretors.end(), vafter.begin(), vafter.end());
        }

        auto vbetween = generateInterpreters(after.first, Type::Group);
        interpretors.insert(interpretors.end(), vbetween.begin(), vbetween.end());
    } else {
        char groupOperator = findOperator(context);
        if (groupOperator == char()) {
            interpretors.push_back(new ValueInterpreter());
        } else {
            OperatorInterpreter* oper = new OperatorInterpreter(groupOperator, type!=Type::None);

            std::pair<std::string, std::string> splitted = split(removeGroup(context), groupOperator);
            auto leftInterpreters = generateInterpreters(splitted.first);
            auto rightInterpreters = generateInterpreters(splitted.second);

            oper->setLeftOperand(getHighestPriority(leftInterpreters));
            oper->setRightOperand(getHighestPriority(rightInterpreters));

            interpretors.push_back(oper);
        }
    }

    return interpretors;
}

Interpreter* CalculatorInterpreter::getHighestPriority(const std::vector<Interpreter*>& groups) const
{
    Interpreter* highest = nullptr;
    for (auto group : groups) {
        if (!highest || group->getPriority() > highest->getPriority())
            highest = group;
    }

    return highest;
}

char CalculatorInterpreter::getHighestPriority(const std::vector<Type>& foundOperators) const
{
    Type highest = Type::Value;
    for (auto ope : foundOperators) {
        if (priorities[ope] > priorities[highest])
            highest = ope;
    }

    return operators[highest];
}

void CalculatorInterpreter::findOperators(std::vector<Type>& operatorsFound, const std::string& s) const
{
    for (auto carac : s) {
        auto find = std::find_if(
            operators.begin(),
            operators.end(),
            [carac](const auto& operators) { return operators.second == carac; });

        if (find != operators.end())
            operatorsFound.push_back(find->first);
    }
}

char CalculatorInterpreter::findOperator(const std::string& s) const
{
    std::vector<Type> operatorsFound = {};
    findOperators(operatorsFound, s);

    return getHighestPriority(operatorsFound);
}

OperatorInterpreter::OperatorInterpreter(char type, bool isGroup) : _isGroup(isGroup)
{
    auto find = std::find_if(
        operators.begin(),
        operators.end(),
        [type](const auto& operators) { return operators.second == type; });

    (find != operators.end()) ? _type = find->first
                              : _type = Type::None;
}

int OperatorInterpreter::interpret(const std::string& context)
{
    std::pair<std::string, std::string> splitted = split(removeGroup(context), getOperator());

    switch (_type)
    {
        case (Type::Add):
            return _leftOperand->interpret(splitted.first) + _rightOperand->interpret(splitted.second);
        case (Type::Substract):
            return _leftOperand->interpret(splitted.first) - _rightOperand->interpret(splitted.second);
        case (Type::Multiply):
            return _leftOperand->interpret(splitted.first) * _rightOperand->interpret(splitted.second);
        case (Type::Divide):
            return _leftOperand->interpret(splitted.first) / _rightOperand->interpret(splitted.second);
        default:
            return 0;
        }
}

int OperatorInterpreter::getPriority() const
{
    if (_isGroup) return priorities[Type::Group];
    return priorities[_type];
}

char OperatorInterpreter::getOperator() const
{
    auto ope = operators.find(_type);
    if (ope != operators.end())
        return ope->second;

    return char();
}

int ValueInterpreter::interpret(const std::string& context)
{
    return std::stoi(context);
}

int ValueInterpreter::getPriority() const
{
    return priorities[Type::Value];
}
