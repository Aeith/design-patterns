#ifndef PROXY_H
#define PROXY_H

#include <iostream>

class Subject {

public:
    Subject() = default;
    virtual ~Subject() = default;

    virtual void stuff() = 0;

};

class RealSubject : public Subject {

public:
    RealSubject() = default;
    virtual ~RealSubject() = default;

    virtual void stuff() override;

};

class Proxy : public Subject
{

public:
    Proxy() = default;
    virtual ~Proxy() = default;

    virtual void stuff() override;

private:
    void preStuff();
    void postStuff();

    RealSubject _subject;

};

#endif // PROXY_H
