#ifndef BEHAVIOURALPATTERNS_H
#define BEHAVIOURALPATTERNS_H

#include "abstract_pattern.h"

class BehaviouralPatterns : public AbstractPattern
{
public:
    BehaviouralPatterns() = default;
    ~BehaviouralPatterns() = default;

    void showPatterns() const;
};

#endif // BEHAVIOURALPATTERNS_H
