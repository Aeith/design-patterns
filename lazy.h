#ifndef LAZY_H
#define LAZY_H

#include <optional>
#include <iostream>

template <class T>
class Lazy {

public:
    // Ctor Dtor
    explicit Lazy() = default;
    virtual ~Lazy() = default;

    /** Operators **/
    T& operator * () {
        if (!_element)
            _element = new T();

        return (*_element);
    }

    T* operator -> () {
        if (!_element)
            _element = new T();

        return _element;
    }

private:
    /** Attributes **/
    T* _element {nullptr};

};

class LazyTest {

public:
    LazyTest() {
        std::cout << "Accessed constructor" << std::endl;
    };
    ~LazyTest() = default;

    /** Accessors **/
    int getValue() const { return _value; }
    void setValue(const int value) { _value = value; }

private:
    int _value {666};

};

#endif // LAZY_H
