#ifndef INTERPRETER_H
#define INTERPRETER_H

#include <string>
#include <map>
#include <regex>

enum class Type { None, Add, Substract, Multiply, Divide, Value, Group };

class Interpreter
{

public:
    Interpreter() = default;
    virtual ~Interpreter() = default;

    virtual int interpret(const std::string& context) = 0;
    virtual int getPriority() const = 0;

};

class CalculatorInterpreter : public Interpreter
{

public:
    CalculatorInterpreter() = default;
    virtual ~CalculatorInterpreter() { delete _expression; }

    virtual int interpret(const std::string& context);
    virtual int getPriority() const;

private:
    std::vector<Interpreter*> generateInterpreters(const std::string& context, Type type = Type::None);
    Interpreter* getHighestPriority(const std::vector<Interpreter*>& groups) const;
    char getHighestPriority(const std::vector<Type>& foundOperators) const;
    void findOperators(std::vector<Type>& operatorsFound, const std::string& s) const;
    char findOperator(const std::string& s) const;

    Interpreter* _expression {nullptr};

};

class OperatorInterpreter : public Interpreter
{

public:
    OperatorInterpreter(char type, bool isGroup);
    virtual ~OperatorInterpreter() {
        delete _leftOperand;
        delete _rightOperand;
    };

    Interpreter* getLeftOperand() const { return _leftOperand; }
    void setLeftOperand(Interpreter* interperter) { _leftOperand = interperter; }

    Interpreter* getRightOperand() const { return _rightOperand; }
    void setRightOperand(Interpreter* interperter) { _rightOperand = interperter; }

    virtual int interpret(const std::string& context);
    virtual int getPriority() const;

    char getOperator() const;
private:
    Interpreter* _leftOperand {nullptr};
    Interpreter* _rightOperand {nullptr};
    Type _type {Type::None};
    bool _isGroup {false};

};

class ValueInterpreter : public Interpreter
{

public:
    ValueInterpreter() {}
    virtual ~ValueInterpreter() = default;

    virtual int interpret(const std::string& context);
    virtual int getPriority() const;

};

#endif // INTERPRETER_H
