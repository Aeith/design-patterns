#include "specification.h"

bool OverDueSpecification::IsSatisfiedBy(Invoice object) const
{
    return object.getOverDue() > 30;
}

bool NoticeSentSpecification::IsSatisfiedBy(Notice object) const
{
    return object.getSentCount() >= 3;
}

bool InCollectionSpecification::IsSatisfiedBy(Agency object) const
{
    return object.isSent();
}
