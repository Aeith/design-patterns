#ifndef CHAINOFRESPONSABILITY_H
#define CHAINOFRESPONSABILITY_H

#include <string>

class Handler
{

public:
    Handler() = default;
    virtual ~Handler() = default;

    virtual Handler* setNext(Handler* next) = 0;
    virtual std::string handle(std::string request) = 0;

};

class AbstractHandler : public Handler
{

public:
    AbstractHandler() = default;
    virtual ~AbstractHandler() = default;

    virtual Handler* setNext(Handler* next) override final;
    virtual std::string handle(std::string request) override;

private:
    Handler* _next {nullptr};

};

class Handler1 : public AbstractHandler {


public:
    Handler1() = default;
    virtual ~Handler1() = default;

    virtual std::string handle(std::string request) override;

};

class Handler2 : public AbstractHandler {


public:
    Handler2() = default;
    virtual ~Handler2() = default;

    virtual std::string handle(std::string request) override;

};

#endif // CHAINOFRESPONSABILITY_H
