#ifndef SERVANT_H
#define SERVANT_H

/**
 * Another implementation is possible ; here the servant is unaware of the service ; it's the user that inject the service to the servant
 * We could also move this behaviour from the user to the service itself : each service is composed of a servant and the user call the
 * service instead of the servant. Pros : Less knowledge for the user. Cons : More to the services.
 **/

#include <iostream>

class Position
{

public:
    Position() {}
    Position(int x, int y) : _x(x), _y(y) {}

    friend std::ostream& operator<<(std::ostream& os, const Position& po) {
        return os << std::to_string(po.getX()) << "," << std::to_string(po.getY());
    }

    int getX() const { return _x; }
    int getY() const { return _y; }

private:
    int _x {0};
    int _y {0};

};

class Movable
{

public:
    Movable() = default;
    virtual ~Movable() = default;

    virtual Position getPosition() const = 0;
    virtual void setPosition(Position pos) = 0;

};

class MoveServant
{

public:
    MoveServant() = default;
    virtual ~MoveServant() = default;

    /** Methods **/
    void moveTo(Movable* service, Position to)
    {
        service->setPosition(to);
    }

    void moveBy(Movable* service, int dx, int dy)
    {
        dx += service->getPosition().getX();
        dy += service->getPosition().getY();
        Position pos(dx, dy);
        service->setPosition(pos);
    }

};

class Ellipse : public Movable
{

public:
    Ellipse() = default;
    virtual ~Ellipse() = default;

    virtual Position getPosition() const { return _pos; }
    virtual void setPosition(Position pos) { _pos = pos; }

private:
    Position _pos;

};

class Rectangle : public Movable
{

public:
    Rectangle() = default;
    virtual ~Rectangle() = default;

    virtual Position getPosition() const { return _pos; }
    virtual void setPosition(Position pos) { _pos = pos; }

private:
    Position _pos;

};

#endif // SERVANT_H
