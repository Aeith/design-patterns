#include "observer.h"

Observer::Observer(Subject* subject) : _subject(subject)
{
    _subject->attach(this);
}

Observer::~Observer()
{
    _subject->detach(this);
}

void ConcreteObserver::update(Subject* s)
{
    std::cout << "Subject notified an update, here's it's name : " << s->getName() << std::endl;
}
