#ifndef DECORATOR_H
#define DECORATOR_H

/**
 * That's a lot of class and inheritance simply for adding extra functionalities
 * Other methods are more scalable and use less code
 **/

#include "composite.h"

class Decorator : public Component
{

public:
    Decorator(Component* wrappee) : _wrappee(wrappee) {};
    virtual ~Decorator() { delete _wrappee; _wrappee = nullptr; };

    virtual std::string execute() const override;

private:
    Component* _wrappee {nullptr};

};

class ConcreteComponent : public Component
{

public:
    ConcreteComponent() = default;
    virtual ~ConcreteComponent() = default;

    virtual std::string execute() const override;

};

class ConcreteDecorator1 : public Decorator
{

public:
    ConcreteDecorator1(Component* wrappee) : Decorator(wrappee) {};
    virtual ~ConcreteDecorator1() = default;

    virtual std::string execute() const override;
    std::string extra1() const;

};

class ConcreteDecorator2 : public Decorator
{

public:
    ConcreteDecorator2(Component* wrappee) : Decorator(wrappee) {};
    virtual ~ConcreteDecorator2() = default;

    virtual std::string execute() const override;
    std::string extra2() const;

};

#endif // DECORATOR_H
