#ifndef STRUCTURALPATTERNS_H
#define STRUCTURALPATTERNS_H

#include "abstract_pattern.h"

class StructuralPatterns : public AbstractPattern
{

public:
    StructuralPatterns() = default;
    ~StructuralPatterns() = default;

    void showPatterns() const;
};

#endif // STRUCTURALPATTERNS_H
