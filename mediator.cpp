#include "mediator.h"

void ButtonComponent::clicked()
{
    std::cout << "Notify from button" << std::endl;
    _mediator->notify(this, "clicked");
}

void TextComponent::textChanged()
{
    std::cout << "Notify from text" << std::endl;
    _mediator->notify(this, "textChanged");
}

void UIMediator::notify(BaseComponent* sender, std::string event) const
{
    if (event == "clicked")
    {
        // activate action somewhere
        std::cout << "Action clicked event" << std::endl;
    }

    if (event == "textChanged")
    {
        std::cout << "Text changed event" << std::endl;
        _button->setEnabled(checkValidity(dynamic_cast<TextComponent*>(sender)->getText()));
    }
}
