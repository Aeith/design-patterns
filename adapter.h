#ifndef ADAPTER_H
#define ADAPTER_H

#include <string>

class Target
{

public:
    Target() = default;
    virtual ~Target() = default;

    virtual std::string targetMethod(int param) const;
};

class Adaptee
{

public:
    Adaptee() = default;
    ~Adaptee() = default;

    std::string adapteeMethod() const;
};

class Adapter : public Target
{

public:
    Adapter(Adaptee adaptee) : _adaptee(adaptee) {};
    ~Adapter() = default;

    virtual std::string targetMethod(int param) const override;

private:
    Adaptee _adaptee;

};

#endif // ADAPTER_H
