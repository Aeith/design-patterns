#ifndef DELEGATE_H
#define DELEGATE_H

/**
 * Seriously, what's the point of the delegee here ?
 * A useless class only calling others shouldn't exists
 **/

#include <string>

class Delegate
{

public:
    Delegate() = default;
    ~Delegate() = default;

    std::string function() const;

};

class Delegee
{

public:
    Delegee(Delegate* delegate) : _delegate(delegate) {};
    ~Delegee() { delete _delegate; _delegate = nullptr; };

    std::string function() const;

private:
    Delegate* _delegate {nullptr};

};

#endif // DELEGATE_H
