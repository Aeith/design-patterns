#include "strategy.h"

std::string Strategy1::algorithm([[maybe_unused]] std::string data) const
{
    return data;
}

std::string Strategy2::algorithm([[maybe_unused]] std::string data) const
{
    std::string res = "";
    for (int i = data.size()-1; i >= 0; i--)
    {
        res += data[i];
    }
    return res;
}

std::string Context::businessLogic() const
{
    if (_strategy)
        return _strategy->algorithm("ceci est une phrase de test");

    return "";
}
